Инструкция(playbook) для Ansible по установке Docker контейнера со стеком ELK на сервер (VDS, dedic) с CentOS, RHEL.
Контейнер готов принимать syslog сразу после установки на 1514 порту.

Исходный код контейнера: https://bitbucket.org/rebirther/elk-auto-docker
Форк от: https://github.com/spujadas/elk-docker

Образ на основе этого кода: https://hub.docker.com/r/rebirther/elk/